// main.c
//
// Hydroponics control program for TI TM4C123GXL, using Keil v5
// Controls various subsystems required to automate hydroponics operation
//
// This file is part of greenwall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-06-12

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/interrupts.h"
#include "lladoware/math.h"
#include "lladoware/output_pins.h"
#include "lladoware/PLL.h"
#include "lladoware/timers.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void programs_init(void);
void programs_update(void);
void servo_command(const uint32_t pin, const uint8_t position);

////////////////////////////////////////////////////////////////////////////////
// Global Constants

#define PROGRAM_FREQUENCY 50  // Hz
#define PROGRAM_PERIOD    SYS_FREQ/PROGRAM_FREQUENCY
#define PROGRAM_PRIORITY  2
#define SERVO1_PIN        PF1
#define SERVO2_PIN        PF2
#define SERVO3_PIN        PF3

////////////////////////////////////////////////////////////////////////////////
// int main()

int main(void) {
  // Initialize all hardware
  PLL_Init();
  programs_init();
  Timer2_Init(PROGRAM_PERIOD, PROGRAM_PRIORITY, &programs_update);

  // All further actions performed by interrupt handlers
  while(1) {
    WaitForInterrupt();
  }
}

////////////////////////////////////////////////////////////////////////////////
// programs_init()
// Run initialization actions for control programs

void programs_init(void) {
  init_pin(SERVO1_PIN, 0);
  init_pin(SERVO2_PIN, 0);
  init_pin(SERVO3_PIN, 0);
}

////////////////////////////////////////////////////////////////////////////////
// programs_update()
// Run update actions for control programs

void programs_update(void) {
  static uint32_t count = 0;
  const uint32_t step = 3;

  count += step;
  const uint8_t position1 = sin8(count);
  const uint8_t position2 = cos8(count);

  servo_command(SERVO1_PIN, position1);
  servo_command(SERVO2_PIN, position2);
}

////////////////////////////////////////////////////////////////////////////////
// servo_command()
// send servo position command

void servo_command(const uint32_t pin, const uint8_t position) {
  const uint32_t duty = (PWM_DUTY_MAX_DEFAULT + PWM_DUTY_MAX_DEFAULT * position / 255) / 20;
  update_pin(pin, duty);
}

////////////////////////////////////////////////////////////////////////////////
// End of file
