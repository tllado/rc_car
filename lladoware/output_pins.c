// lladoware_output_pins.c
//
// Onboard LED control for TI TM4C123GXL using Keil v5
// A simple library that initializes and updates all GPIO and PWM output pins
// on a TI TM4C123GXL microcontroller
//
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-02-27

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "interrupts.h"
#include "output_pins.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

uint32_t init_pin_gpio(uint32_t pin_index);
uint32_t init_pin_pwm(uint32_t pin_index, uint32_t period);
uint32_t update_pin_gpio(uint32_t pin_index, uint32_t command);
uint32_t update_pin_pwm(uint32_t pin_index, uint32_t command);

////////////////////////////////////////////////////////////////////////////////
// Constants

#define NUM_PINS 35

#define PORT_A  0x40004000  // GPIO_PORTA_DATA_BITS_R
#define PORT_B  0x40005000  // GPIO_PORTB_DATA_BITS_R
#define PORT_C  0x40006000  // GPIO_PORTC_DATA_BITS_R
#define PORT_D  0x40007000  // GPIO_PORTD_DATA_BITS_R
#define PORT_E  0x40024000  // GPIO_PORTE_DATA_BITS_R
#define PORT_F  0x40025000  // GPIO_PORTF_DATA_BITS_R

#define PIN_0   0x01<<(2+0)
#define PIN_1   0x01<<(2+1)
#define PIN_2   0x01<<(2+2)
#define PIN_3   0x01<<(2+3)
#define PIN_4   0x01<<(2+4)
#define PIN_5   0x01<<(2+5)
#define PIN_6   0x01<<(2+6)
#define PIN_7   0x01<<(2+7)

typedef volatile uint32_t* register_type;

#define INVALID_REG (*((volatile uint32_t *)0))
#define INVALID     0

#define RED_LED   PF1
#define BLUE_LED  PF3
#define GREEN_LED PF2

// All of the following arrays are arranged in the form:
//
//          PA2 PA3 PA4 PA5 PA6 PA7
//  PB0 PB1 PB2 PB3 PB4 PB5 PB6 PB7
//                  PC4 PC5 PC6 PC7
//  PD0 PD1 PD2 PD3         PD6 PD7
//  PE0 PE1 PE2 PE3 PE4 PE5
//  PF0 PF1 PF2 PF3 PF4
//
// It's not very readable, but hopefully no one needs to read this.

const register_type GPIO_AFSEL_REG[NUM_PINS] = {
                                            &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R,
  &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R,
                                                                                      &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R,
  &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R,                                           &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R,
  &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R,
  &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R
};

const register_type GPIO_AMSEL_REG[NUM_PINS] = {
                                            &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R,
  &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R,
                                                                                      &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R,
  &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R,                                           &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R,
  &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R,
  &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R
};

const register_type GPIO_DATA_REG[NUM_PINS] = {
                                                                              (volatile uint32_t*)(PORT_A | PIN_2), (volatile uint32_t*)(PORT_A | PIN_3), (volatile uint32_t*)(PORT_A | PIN_4), (volatile uint32_t*)(PORT_A | PIN_5), (volatile uint32_t*)(PORT_A | PIN_6), (volatile uint32_t*)(PORT_A | PIN_7),
  (volatile uint32_t*)(PORT_B | PIN_0), (volatile uint32_t*)(PORT_B | PIN_1), (volatile uint32_t*)(PORT_B | PIN_2), (volatile uint32_t*)(PORT_B | PIN_3), (volatile uint32_t*)(PORT_B | PIN_4), (volatile uint32_t*)(PORT_B | PIN_5), (volatile uint32_t*)(PORT_B | PIN_6), (volatile uint32_t*)(PORT_B | PIN_7),
                                                                                                                                                          (volatile uint32_t*)(PORT_C | PIN_4), (volatile uint32_t*)(PORT_C | PIN_5), (volatile uint32_t*)(PORT_C | PIN_6), (volatile uint32_t*)(PORT_C | PIN_7),
  (volatile uint32_t*)(PORT_D | PIN_0), (volatile uint32_t*)(PORT_D | PIN_1), (volatile uint32_t*)(PORT_D | PIN_2), (volatile uint32_t*)(PORT_D | PIN_3),                                                                             (volatile uint32_t*)(PORT_D | PIN_6), (volatile uint32_t*)(PORT_D | PIN_7),
  (volatile uint32_t*)(PORT_E | PIN_0), (volatile uint32_t*)(PORT_E | PIN_1), (volatile uint32_t*)(PORT_E | PIN_2), (volatile uint32_t*)(PORT_E | PIN_3), (volatile uint32_t*)(PORT_E | PIN_4), (volatile uint32_t*)(PORT_E | PIN_5),
  (volatile uint32_t*)(PORT_F | PIN_0), (volatile uint32_t*)(PORT_F | PIN_1), (volatile uint32_t*)(PORT_F | PIN_2), (volatile uint32_t*)(PORT_F | PIN_3), (volatile uint32_t*)(PORT_F | PIN_4)
};

const register_type GPIO_DEN_REG[NUM_PINS] = {
                                        &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R,
  &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R,
                                                                              &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R,
  &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R,                                       &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R,
  &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R,
  &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R
};

const register_type GPIO_DIR_REG[NUM_PINS] = {
                                        &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R,
  &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R,
                                                                              &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R,
  &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R,                                       &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R,
  &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R,
  &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R
};

const register_type GPIO_PCTL_REG[NUM_PINS] = {
                                          &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R,
  &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R,
                                                                                  &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R,
  &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R,                                         &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R,
  &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R,
  &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R
};

const uint32_t PIN_NUM[NUM_PINS] = {
        2, 3, 4, 5, 6, 7,
  0, 1, 2, 3, 4, 5, 6, 7,
              4, 5, 6, 7,
  0, 1, 2, 3,       6, 7,
  0, 1, 2, 3, 4, 5,
  0, 1, 2, 3, 4
};

const uint32_t PWM_AF_NUM[NUM_PINS] = {
                    INVALID, INVALID, INVALID, INVALID, 5,       5,
  INVALID, INVALID, INVALID, INVALID, 4,       4,       4,       4,
                                      4,       4,       INVALID, INVALID,
  5,       5,       INVALID, INVALID,                   INVALID, INVALID,
  INVALID, INVALID, INVALID, INVALID, 4,4,
  5,       5,       5,       5,       INVALID
};

const uint32_t PWM_BLK_ENBL[NUM_PINS] = {
                                      INVALID,          INVALID,          INVALID,          INVALID,          PWM_1_CTL_ENABLE, PWM_1_CTL_ENABLE,
  INVALID,          INVALID,          INVALID,          INVALID,          PWM_1_CTL_ENABLE, PWM_1_CTL_ENABLE, PWM_0_CTL_ENABLE, PWM_0_CTL_ENABLE,
                                                                          PWM_3_CTL_ENABLE, PWM_3_CTL_ENABLE, INVALID,          INVALID,
  PWM_0_CTL_ENABLE, PWM_0_CTL_ENABLE, INVALID,          INVALID,                                              INVALID,          INVALID,
  INVALID,          INVALID,          INVALID,          INVALID,          PWM_2_CTL_ENABLE, PWM_2_CTL_ENABLE,
  PWM_2_CTL_ENABLE, PWM_2_CTL_ENABLE, PWM_3_CTL_ENABLE, PWM_3_CTL_ENABLE, INVALID
};

const register_type PWM_CMP_REG[NUM_PINS] = {
                                  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_CMPA_R, &PWM1_1_CMPB_R,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_CMPA_R, &PWM0_1_CMPB_R, &PWM0_0_CMPA_R, &PWM0_0_CMPB_R,
                                                                  &PWM0_3_CMPA_R, &PWM0_3_CMPB_R, &INVALID_REG,   &INVALID_REG,
  &PWM1_0_CMPA_R, &PWM1_0_CMPB_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_CMPA_R, &PWM0_2_CMPB_R,
  &PWM1_2_CMPA_R, &PWM1_2_CMPB_R, &PWM1_3_CMPA_R, &PWM1_3_CMPB_R, &INVALID_REG
};

const register_type PWM_CTL_REG[NUM_PINS] = {
                                &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM1_1_CTL_R, &PWM1_1_CTL_R,
  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM0_1_CTL_R, &PWM0_1_CTL_R, &PWM0_0_CTL_R, &PWM0_0_CTL_R,
                                                              &PWM0_3_CTL_R, &PWM0_3_CTL_R, &INVALID_REG,  &INVALID_REG,
  &PWM1_0_CTL_R, &PWM1_0_CTL_R, &INVALID_REG,  &INVALID_REG,                                &INVALID_REG,  &INVALID_REG,
  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM0_2_CTL_R, &PWM0_2_CTL_R,
  &PWM1_2_CTL_R, &PWM1_2_CTL_R, &PWM1_3_CTL_R, &PWM1_3_CTL_R, &INVALID_REG
};

const register_type PWM_ENBL_REG[NUM_PINS] = {
                                  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_ENABLE_R, &PWM1_ENABLE_R,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_ENABLE_R, &PWM0_ENABLE_R, &PWM0_ENABLE_R, &PWM0_ENABLE_R,
                                                                  &PWM0_ENABLE_R, &PWM0_ENABLE_R, &INVALID_REG,   &INVALID_REG,
  &PWM1_ENABLE_R, &PWM1_ENABLE_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_ENABLE_R, &PWM0_ENABLE_R,
  &PWM1_ENABLE_R, &PWM1_ENABLE_R, &PWM1_ENABLE_R, &PWM1_ENABLE_R, &INVALID_REG
};

const register_type PWM_GEN_REG[NUM_PINS] = {
                                  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_GENA_R, &PWM1_1_GENB_R,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_GENA_R, &PWM0_1_GENB_R, &PWM0_0_GENA_R, &PWM0_0_GENB_R,
                                                                  &PWM0_3_GENA_R, &PWM0_3_GENB_R, &INVALID_REG,   &INVALID_REG,
  &PWM1_0_GENA_R, &PWM1_0_GENB_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_GENA_R, &PWM0_2_GENB_R,
  &PWM1_2_GENA_R, &PWM1_2_GENB_R, &PWM1_3_GENA_R, &PWM1_3_GENB_R, &INVALID_REG
};

const register_type PWM_LOAD_REG[NUM_PINS] = {
                                  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_LOAD_R,   &PWM1_1_LOAD_R,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_LOAD_R,   &PWM0_1_LOAD_R, &PWM0_0_LOAD_R, &PWM0_0_LOAD_R,
                                                                  &PWM0_3_LOAD_R, &PWM0_3_LOAD_R, &INVALID_REG,   &INVALID_REG,
  &PWM1_0_LOAD_R, &PWM1_0_LOAD_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
  &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_LOAD_R, &PWM0_2_LOAD_R,
  &PWM1_2_LOAD_R, &PWM1_2_LOAD_R, &PWM1_3_LOAD_R, &PWM1_3_LOAD_R, &INVALID_REG
};

const uint32_t PWM_MOD_ENBL[NUM_PINS] = {
                                        INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM2EN, PWM_ENABLE_PWM3EN,
  INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM2EN, PWM_ENABLE_PWM3EN, PWM_ENABLE_PWM0EN, PWM_ENABLE_PWM1EN,
                                                                              PWM_ENABLE_PWM6EN, PWM_ENABLE_PWM7EN, INVALID,           INVALID,
  PWM_ENABLE_PWM0EN, PWM_ENABLE_PWM1EN, INVALID,           INVALID,                                                 INVALID,           INVALID,
  INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM4EN, PWM_ENABLE_PWM5EN,
  PWM_ENABLE_PWM4EN, PWM_ENABLE_PWM5EN, PWM_ENABLE_PWM6EN, PWM_ENABLE_PWM7EN, INVALID
};

const uint32_t PWM_TRGR[NUM_PINS] = {
                                                                                                            INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_1_GENA_ACTCMPAD_ONE | PWM_1_GENA_ACTLOAD_ZERO), (PWM_1_GENB_ACTCMPBD_ONE | PWM_1_GENB_ACTLOAD_ZERO),
  INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_1_GENA_ACTCMPAD_ONE | PWM_1_GENA_ACTLOAD_ZERO), (PWM_1_GENB_ACTCMPBD_ONE | PWM_1_GENB_ACTLOAD_ZERO), (PWM_0_GENA_ACTCMPAD_ONE | PWM_0_GENA_ACTLOAD_ZERO), (PWM_0_GENB_ACTCMPBD_ONE | PWM_0_GENB_ACTLOAD_ZERO),
                                                                                                                                                                                                                      (PWM_3_GENA_ACTCMPAD_ONE | PWM_3_GENA_ACTLOAD_ZERO), (PWM_3_GENB_ACTCMPBD_ONE | PWM_3_GENB_ACTLOAD_ZERO), INVALID,                                             INVALID,
  (PWM_0_GENA_ACTCMPAD_ONE | PWM_0_GENA_ACTLOAD_ZERO), (PWM_0_GENB_ACTCMPBD_ONE | PWM_0_GENB_ACTLOAD_ZERO), INVALID,                                             INVALID,                                                                                                                                                       INVALID,                                             INVALID,
  INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_2_GENA_ACTCMPAD_ONE | PWM_2_GENA_ACTLOAD_ZERO), (PWM_2_GENB_ACTCMPBD_ONE | PWM_2_GENB_ACTLOAD_ZERO),
  (PWM_2_GENA_ACTCMPAD_ONE | PWM_2_GENA_ACTLOAD_ZERO), (PWM_2_GENB_ACTCMPBD_ONE | PWM_2_GENB_ACTLOAD_ZERO), (PWM_3_GENA_ACTCMPAD_ONE | PWM_3_GENA_ACTLOAD_ZERO), (PWM_3_GENB_ACTCMPBD_ONE | PWM_3_GENB_ACTLOAD_ZERO), INVALID
};

const uint32_t SYSCTL_PRGPIO[NUM_PINS] = {
                                      SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0,
  SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1,
                                                                          SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2,
  SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3,                                     SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3,
  SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4,
  SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5
};

const uint32_t SYSCTL_RCGCGPIO[NUM_PINS] = {
                                          SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0,
  SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1,
                                                                                  SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2,
  SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3,                                         SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3,
  SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4,
  SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5
};

const uint32_t SYSCTL_RCGCPWM[NUM_PINS] = {
                                        INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1,
  INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0,
                                                                              SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, INVALID,           INVALID,
  SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, INVALID,           INVALID,                                                 INVALID,           INVALID,
  INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0,
  SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, INVALID
};

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t global_pwm_period[NUM_PINS];

////////////////////////////////////////////////////////////////////////////////
// init_pin()
// Initializes one pin as either PWM and GPIO or as just GPIO
// If desired to use pin as GPIO or with default PWM frequency of 1kHz, then
// period should be set as 0
// Returns 1 (success) or 0 (failure)

uint32_t init_pin(uint32_t pin_index, uint32_t period){
  uint32_t success = 0;

  if(PWM_AF_NUM[pin_index] == INVALID) {
    success = init_pin_gpio(pin_index);
  } else {
    success = init_pin_pwm(pin_index, period);
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_gpio()
// Initializes one pin as GPIO and sets it to LOW
// Returns 1 (success) or 0 (failure)

uint32_t init_pin_gpio(uint32_t pin_index){
  uint32_t success = 1;

  DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO[pin_index];            // Enable GPIO clock
    while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO[pin_index]) == 0) {} // Wait until peripheral ready

    *GPIO_AFSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);      // disable alt functions
    *GPIO_PCTL_REG[pin_index]  &= ~(0x0F << (4*PIN_NUM[pin_index]));  // enable GPIO function
    *GPIO_AMSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);      // disable analog function
    *GPIO_DEN_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);      // enable digital I/O
    *GPIO_DIR_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);      // set GPIO direction to out
  EnableInterrupts();

  *GPIO_DATA_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);  // Initialize pin to LOW

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_pwm()
// Initializes one pin as PWM, prepares it for quick switching to GPIO if
// necessary, and sets duty to 0

uint32_t init_pin_pwm(uint32_t pin_index, uint32_t period){
  uint32_t success = 1;

  if(period > PWM_PERIOD_MAX) {
    success = 0;
  } else {
    if(period == 0) {
      period = PWM_PERIOD_DEFAULT;
    }

    global_pwm_period[pin_index] = period;

    DisableInterrupts();
      SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM[pin_index];              // Initialize PWM clock
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO[pin_index];            // Initialize GPIO clock
      while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO[pin_index]) == 0) {} // Wait until peripheral ready
      SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;                       // Set PWM clock w.r.t. system clock
      SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
      SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;

      *GPIO_AFSEL_REG[pin_index] |=  (0x01 << PIN_NUM[pin_index]);                        // enable alt functions
      *GPIO_PCTL_REG[pin_index] &=  ~(0x0F << (4*PIN_NUM[pin_index]));                    // disable other alt functions
      *GPIO_PCTL_REG[pin_index] |=   (PWM_AF_NUM[pin_index] << (4*PIN_NUM[pin_index]));   // enable PWM function
      *GPIO_AMSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);                        // disable analog function
      *GPIO_DEN_REG[pin_index] |=    (0x01 << PIN_NUM[pin_index]);                        // enable digital I/O
      *GPIO_DIR_REG[pin_index] |=    (0x01 << PIN_NUM[pin_index]);                        // set GPIO direction to out

      *PWM_CTL_REG[pin_index] = 0;                            // set countdown mode
      *PWM_GEN_REG[pin_index] = PWM_TRGR[pin_index];          // define signal triggers
      *PWM_LOAD_REG[pin_index] = period;                      // set counter reset values (period)
      *PWM_CMP_REG[pin_index] = 0;                            // init comparator values (duty)
      *PWM_CTL_REG[pin_index] |= PWM_BLK_ENBL[pin_index];     // enable block
      *PWM_ENBL_REG[pin_index] |= PWM_MOD_ENBL[pin_index];    // enable module
    EnableInterrupts();
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_init()
// Initialize PF1-3 as PWM output
// Returns 1 (success) or 0 (failure)

uint32_t onboard_leds_init(void) {
  uint32_t success = 1;

  success = success && init_pin(RED_LED, PWM_PERIOD_DEFAULT);
  success = success && init_pin(BLUE_LED, PWM_PERIOD_DEFAULT);
  success = success && init_pin(GREEN_LED, PWM_PERIOD_DEFAULT);

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_update()
// Updates duty cycles for onboard LEDs
// Returns 1 (success) or 0 (failure)

uint32_t onboard_leds_update(
    const uint32_t red_command, const uint32_t blue_command, const uint32_t green_command){
  uint32_t success = 1;

  if(red_command != PIN_STATE_UNCHANGED) {
    success = success && update_pin(RED_LED, red_command);
  }
  if(blue_command != PIN_STATE_UNCHANGED) {
    success = success && update_pin(BLUE_LED, blue_command);
  }
  if(green_command != PIN_STATE_UNCHANGED) {
    success = success && update_pin(GREEN_LED, green_command);
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// update_pin()
// Updates GPIO state or PWM duty
// Returns 1 (success) or 0 (failure)

uint32_t update_pin(uint32_t pin_index, uint32_t command){
  uint32_t success = 1;

  if(command == PIN_STATE_UNCHANGED) {
    // do nothing
  } else if((command == PIN_STATE_HIGH) || (command == PIN_STATE_LOW)) {
    success = update_pin_gpio(pin_index, command);
  } else {
    success = update_pin_pwm(pin_index, command);
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// update_pin_gpio()
// Reinitializes pin as GPIO (faster than checking state) and updates state
// Returns 1 (success) or 0 (failure)

uint32_t update_pin_gpio(uint32_t pin_index, uint32_t command){
  uint32_t success = 1;

  DisableInterrupts();
    *GPIO_AFSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);    // disable alt functions
    *GPIO_PCTL_REG[pin_index] &= ~(0x0F << (4*PIN_NUM[pin_index])); // enable GPIO function
  EnableInterrupts();

  // Set new GPIO state
  if(command == PIN_STATE_HIGH) {
    *GPIO_DATA_REG[pin_index] |= (0x01 << PIN_NUM[pin_index]);  // set pin value
  } else {
    *GPIO_DATA_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]); // clear pin value
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// update_pin_pwn()
// Reinitializes pin as PWM (faster than checking state) and updates duty
// Returns 1 (success) or 0 (failure)

uint32_t update_pin_pwm(uint32_t pin_index, uint32_t command){
  uint32_t success = 1;

  if((PWM_AF_NUM[pin_index] == INVALID) || (command >= global_pwm_period[pin_index])) {
    success = 0;
  } else {
    DisableInterrupts();
      *GPIO_AFSEL_REG[pin_index] |= (0x01 << PIN_NUM[pin_index]);                     // enable alt functions
      *GPIO_PCTL_REG[pin_index] |= (PWM_AF_NUM[pin_index] << (4*PIN_NUM[pin_index])); // enable PWM function
    EnableInterrupts();

    *PWM_CMP_REG[pin_index] = command; // Set new PWM duty
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
