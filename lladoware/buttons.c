// lladoware_buttons.c
//
// Onboard button management for TI TM4C123GXL using Keil v5
// A simple library that initializes and handles onboard buttons
// on a TI TM4C123GXL microcontroller
//
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-06

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "buttons.h"
#include "interrupts.h"
#include "system_config.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define BL  0x10  // PF4
#define BR  0x01  // PF0

#define MAX_PRIORITY  7

////////////////////////////////////////////////////////////////////////////////
// Global Variables

void(*left_task)(void);
void(*right_task)(void);
uint32_t greatest_priority = MAX_PRIORITY;

////////////////////////////////////////////////////////////////////////////////
// buttons_disable()
// Disables built-in buttons

void buttons_disable(void) {
	NVIC_DIS0_R = (1 << 30);  // disable IRQ 30 in NVIC
}

////////////////////////////////////////////////////////////////////////////////
// buttons_enable()
// Enables built-in buttons

void buttons_enable(void) {
	NVIC_EN0_R = (1 << 30); // enable IRQ 30 in NVIC
}

////////////////////////////////////////////////////////////////////////////////
// button_left_init()
// Initializes left built-in button with user-provided task

uint32_t button_left_init(const uint32_t priority, void(*task)(void)){
  uint32_t success = 1;
  if(priority > MAX_PRIORITY) {
    success = 0;
  } else {
    if(priority < greatest_priority) {
      greatest_priority = priority;
    }

    left_task = task;

    DisableInterrupts();
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5; // Initialize GPIO clock
      while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R5) == 0) {} // Wait until peripheral ready
      GPIO_PORTF_DIR_R &= ~BL;          // set GPIO direction to input
      GPIO_PORTF_AFSEL_R &= ~BL;        // disable alt functions
      GPIO_PORTF_DEN_R |= BL;           // enable digital I/O
      GPIO_PORTF_PCTL_R &= ~0x000F0000; // enable GPIO function
      GPIO_PORTF_AMSEL_R &= ~BL;        // disable analog function
      GPIO_PORTF_PUR_R |= BL;           // enable pull-up resistor
      GPIO_PORTF_IS_R &= ~BL;           // set edge-sensitive
      GPIO_PORTF_IBE_R &= ~BL;          // set one edge only
      GPIO_PORTF_IEV_R &= ~BL;          // set falling edge
      GPIO_PORTF_ICR_R = BL;            // clear interrupt
      GPIO_PORTF_IM_R |= BL;            // remove interrupt mask
      NVIC_PRI7_R &= ~0x00FF0000;       // clear previous priority
      NVIC_PRI7_R |= (greatest_priority << 13); // set priority
      NVIC_EN0_R = (1 << 30);           // enable interrupt
    EnableInterrupts();
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// button_right_init()
// Initializes right built-in button with user-provided task

uint32_t button_right_init(const uint32_t priority, void(*task)(void)){
  uint32_t success = 1;
  if(priority > MAX_PRIORITY) {
    success = 0;
  } else {
    if(priority < greatest_priority) {
      greatest_priority = priority;
    }

    right_task = task;

    DisableInterrupts();
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5; // Initialize GPIO clock
      while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R5) == 0) {} // Wait until peripheral ready
      GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;  // unlock GPIO Port F Commit Register
      GPIO_PORTF_CR_R |= (BL|BR);       // enable commit for PF4 and PF0
      GPIO_PORTF_DIR_R &= ~BR;          // set GPIO direction to input
      GPIO_PORTF_AFSEL_R &= ~BR;        // disable alt functions
      GPIO_PORTF_DEN_R |= BR;           // enable digital I/O
      GPIO_PORTF_PCTL_R &= ~0x0000000F; // enable GPIO function
      GPIO_PORTF_AMSEL_R &= ~BR;        // disable analog function
      GPIO_PORTF_PUR_R |= BR;           // enable pull-up resistor
      GPIO_PORTF_IS_R &= ~BR;           // set edge-sensitive
      GPIO_PORTF_IBE_R &= ~BR;          // set one edge only
      GPIO_PORTF_IEV_R &= ~BR;          // set falling edge
      GPIO_PORTF_ICR_R = BR;            // clear interrupt
      GPIO_PORTF_IM_R |= BR;            // remove interrupt mask
      NVIC_PRI7_R &= ~0x00FF0000;       // clear previous priority
      NVIC_PRI7_R |= (greatest_priority << 13); // set priority
      NVIC_EN0_R = (1 << 30);           // enable interrupt
    EnableInterrupts();
  }

  return success;
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Runs user-provided tasks when buttons interrupt

void GPIOPortF_Handler(void) {
  uint32_t count = SYS_FREQ/1000/6*DEBOUNCE_LENGTH;
  while(count-- > 0) {}

	if(GPIO_PORTF_RIS_R & BR) { // check PF0
    GPIO_PORTF_ICR_R = BR;    // acknowledge interrupt
    right_task();
	}
	if(GPIO_PORTF_RIS_R & BL) { // check PF4
    GPIO_PORTF_ICR_R = BL;    // acknowledge interrupt
    left_task();
	}
}

////////////////////////////////////////////////////////////////////////////////
// End of file
