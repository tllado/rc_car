// lladoware_output_pins.h
//
// Onboard LED control for TI TM4C123GXL using Keil v5
// A simple library that initializes and updates all GPIO and PWM output pins
// on a TI TM4C123GXL microcontroller
//
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-02-27

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "system_config.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define PWM_PERIOD_MAX    65535
#define PWM_FREQ_DEFAULT  50  // Hz

#define PWM_PERIOD_DEFAULT    SYS_FREQ/PWM_DIV/PWM_FREQ_DEFAULT
#define PWM_DUTY_MAX_DEFAULT  (PWM_PERIOD_DEFAULT - 1)

// GPIO States
#define PIN_STATE_LOW       (PWM_PERIOD_MAX + 1)
#define PIN_STATE_HIGH      (PWM_PERIOD_MAX + 2)
#define PIN_STATE_UNCHANGED (PWM_PERIOD_MAX + 3)

// Pin Indices
enum GPIO_PINS{
                      PA2 = 0,  PA3 = 1,  PA4 = 2,  PA5 = 3,  PA6 = 4,  PA7 = 5,
  PB0 = 6,  PB1 = 7,  PB2 = 8,  PB3 = 9,  PB4 = 10, PB5 = 11, PB6 = 12, PB7 = 13,
                                          PC4 = 14, PC5 = 15, PC6 = 16, PC7 = 17,
  PD0 = 18, PD1 = 19, PD2 = 20, PD3 = 21,                     PD6 = 22, PD7 = 23,
  PE0 = 24, PE1 = 25, PE2 = 26, PE3 = 27, PE4 = 28, PE5 = 29,
  PF0 = 30, PF1 = 31, PF2 = 32, PF3 = 33, PF4 = 34
};

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

// init_pin()
// Initializes one pin as either PWM and GPIO or as just GPIO
// If desired to use pin as GPIO or with default PWM frequency of 1kHz, then
// period should be set as 0
uint32_t init_pin(uint32_t pin_index, uint32_t period);

// onboard_leds_init()
// Initializes Pins F1-3 for PWM output to run onboard LEDs
uint32_t onboard_leds_init(void);

// onboard_leds_update()
// Update duties for LEDs
uint32_t onboard_leds_update(uint32_t red_command, uint32_t green_command, uint32_t blue_command);

// update_pin()
// Updates GPIO state or PWM duty
uint32_t update_pin(uint32_t pin_index, uint32_t command);

////////////////////////////////////////////////////////////////////////////////
// End of file
